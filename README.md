# Set up a Project and Its Dependencies


## Task
### Please do the following steps:

1. Create a package.json file
2. Install TypeScript as a dev dependency
3. Install rxjs library as a main dependency
4. Install json-server as a global package
5. Create a db.json file and add the following content to it:

```
{ 
  "tasks": [ 
    { 
      "id": 1, 
      "action": "Estimate", 
      "priority": 3, 
      "estHours": 8 
    }, 
    { 
      "id": 2, 
      "action": "Create", 
      "priority": 2, 
      "estHours": 8 
    }
  ] 
}
```
- add the following command to the scripts section of the package json "start": "json-server --watch db.json",
6. Provide the mentor with:
- - a screenshot of the content of the package.json file
- - a screenshot of the content of a terminal window after executing the command "npm start"
- - a link to the gitlab project with installed dependencies and dev-dependencies